
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import AnimeList from 'components/AnimeList';
import AnimeDetail from 'components/AnimeDetail';


import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter basename='kds-mouchot-showcase'>
          <Switch>
            <Route path="/:animeId">
              <AnimeDetail/>
            </Route>
            <Route path="/">
              <AnimeList/>
            </Route>
          </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
