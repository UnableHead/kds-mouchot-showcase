import {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import ReactLoading from 'react-loading';
import AniApi from 'services/query/AniApi';
import AnimePreview from "./AnimePreview";

export default function AnimeDetail({props}){
  const { animeId } = useParams();
  const [animeData, setAnimeData] = useState(null);

  useEffect(() => {
    const [query, controller] = AniApi.get(animeId);
    query.then((suppliedData) => {
      setAnimeData(suppliedData.data);
    })
      .catch(() => {});
    return () => controller.abort();
  }, [animeId]);

  return (
    <div className="AnimeDetail">
      <div className="ReturnLink">
        <Link to="/">Go back</Link>
      </div>
      {!animeData
        ? <ReactLoading type="bars" color="blue" height={667} width={375}/>
        : <AnimePreview {...animeData}/>

      }
    </div>
  );
}