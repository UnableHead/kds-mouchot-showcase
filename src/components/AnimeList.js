import {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import {DataGrid} from '@material-ui/data-grid';
import TablePagination from '@material-ui/core/TablePagination';
import AniApi from 'services/query/AniApi';

const columns = [
  { field: 'id', headerName: 'ID', sortable: false, flex: 1},
  {
    field: 'titles',
    headerName: 'Title',
    sortable: false,
    flex: 1,
    valueGetter: (params) => (params.value.en),
  },
  {field: 'season_year', headerName: 'Year', sortable: false, flex: 1},
  {
    field: 'genres',
    headerName: 'Genres',
    sortable: false,
    flex: 1,
    valueGetter: (params) => (params.value.slice(0, 3).join()),
  },
];

export default function AnimeList(){
  const history = useHistory();
  const [rowCount, setRowCount] = useState(1);
  const [pageIndex, setPageIndex] = useState(0);
  const [numberByPage, setNumberByPage] = useState(10);
  const [animeList, setAnimeList] = useState([]);

  const handleRowClick = (row) => {
    history.push(`/${row.id}`);
  };

  const handlePageChange = (event, index) => {
    setPageIndex(index);
  };

  const handleNumberPageChange = (event) => {
    const newNumberByPage = Number.parseInt(event.target.value, 10);
    setPageIndex(Math.floor(pageIndex * numberByPage / newNumberByPage));
    setNumberByPage(newNumberByPage);
  };

  useEffect(() => {
    const [query, controller] = AniApi.search({page: pageIndex + 1, per_page: numberByPage});
    query.then((suppliedData) => {
      if(!suppliedData || !suppliedData.data || !Array.isArray(suppliedData.data.documents)){
        throw suppliedData;
      }
      setRowCount(suppliedData.data.count);
      setAnimeList(suppliedData.data.documents);
    })
      .catch(() => {});
    return () => controller.abort();
  }, [pageIndex, numberByPage]);

  return (
    <div className='PageAnimeList'>
      <h2>Anime Table</h2>
      <div className='GridWrapper'>
        <DataGrid
          columns={columns}
          rows={animeList}
          onRowClick={handleRowClick}
          disableColumnFilter={true}
          hideFooter={true}
        />
      </div>
      <div className='FooterTable'>
        <TablePagination
          page={pageIndex}
          count={rowCount}
          onPageChange={handlePageChange}
          rowsPerPage={numberByPage}
          onRowsPerPageChange={handleNumberPageChange}
        />
      </div>
    </div>
    
  )
};