import { makeStyles } from '@material-ui/core/styles';
import {Card, CardContent, CardMedia, Typography} from '@material-ui/core';

function getTitle(titles){
  if(titles.en){
    return titles.en;
  }
  const lang = Object.keys(titles);
  if(lang.length > 0){
    return titles[lang[0]];
  } else {
    return 'No title';
  }
}

const useStyles = makeStyles({
  root: {
    width: 500,
  },
  media: {
    height: 180,
  },
});

export default function AnimePreview({titles, cover_image, banner_image, season_year, genres, descriptions}){
  const title = getTitle(titles);
  const description = getTitle(descriptions);
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {title}
        </Typography>
      </CardContent>
      <CardMedia
        className={classes.media}
        image={banner_image}
        title={title}
      />
      <CardContent>
        <Typography>{season_year}</Typography>
        <Typography fontStyle="italic">{genres.slice(0, 3).join()}</Typography>
        <img src={cover_image} alt={title}/>
        <Typography align="justify">{description}</Typography>
      </CardContent>
    </Card>
  );
}