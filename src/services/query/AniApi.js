import QueryManager from "./QueryManager";

const _ = {
  apiURL: 'https://api.aniapi.com/',
  jwt: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU3IiwibmJmIjoxNjI3ODI3MzQxLCJleHAiOjE2MzA0MTkzNDEsImlhdCI6MTYyNzgyNzM0MX0.4fJR2GIUIypGov_FmEVQLzvkTR80Nf-aFei3xwgjrsg',
};

let aniApiInstance = null

function initAniQuery() {
  if (aniApiInstance === null) {
    aniApiInstance = new QueryManager(_.apiURL, {'Authorization': `Bearer ${_.jwt}`});
  }
  return aniApiInstance;
}

function get(id) {
  return initAniQuery().queryJson('v1/anime/' + id);
}

function search(params) {
  const urlSearchParams = new URLSearchParams({per_page: 10, ...params});
  return initAniQuery().queryJson('v1/anime?' + urlSearchParams.toString());
}

const AniApi = {
  get,
  search
};

export default AniApi;