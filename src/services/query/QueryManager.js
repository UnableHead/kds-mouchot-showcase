
class QueryManager {
  constructor(baseUrl, headers) {
    this.baseUrl = baseUrl;
    this.param = {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            ...headers,
        }
    };
  }

  _makeAbortController() {
    if (typeof AbortController !== 'function') { // mock abort
      return {
        signal: () => {},
        abort: () => {},
      };
    }
    return new AbortController();
  }

  query(path = '', requestParam = {}) {
    // AbortController
    const controller = this._makeAbortController();
    requestParam.signal = controller.signal;
    // Query
    const query = fetch(this.baseUrl + path, {
      ...this.param,
      ...requestParam,
    })
      .then((result) => {
        if (!result || typeof result !== 'object') {
          throw result;
        }
        return result;
      });
    return [query, controller];
  }

  queryJson(path, requestParam) {
    const [query, controller] = this.query(path, requestParam);
    const queryJSON = query.then((response) => {
      return response.json();
    });
    return [queryJSON, controller];
  }

}

export default QueryManager;
